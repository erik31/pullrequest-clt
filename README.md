#PullRequest-CLT (pr_clt)

This is a Command Line Tool that searches for Pull Requests in Bitbucket account.


##Installation

In order to install this program, you have to write next command in cmd or terminal:

```
   pip install pr_clt
```
or
```
   pip3 install pr_clt
```


##Usage

You can invoke PullRequest-CLT with ``pr_clt`` command, being in any directory of your cmd or terminal

```
   pr_cli [-h --help] <username> <repository> [-p --password PASSWORD] [-b --browser]
```

Required arguments:

* username                    - it is your BitBucket account to which Pull Requests could be sent.
* repository                  - it is BitBucket repository in which you want to search for Pull Requests.

Options:

* -h --help                   - full help information guide.
* -b --browser                - will open link in new tab of browser.
* -p --password PASSWORD      - password of BitBucket account that has to be used for private repositories.


##Possible outputs:

```
Repository username/repository not found.
Incorrect username or repository.
```
This output will be in case when status code of request is 404. It means that provided username or repository does not exist.


```
This is private repository. Please enter password using --password or -p
```
This output will be in case when status code of request is 403. It means that provided repository is forbidden and password is required.


```
Incorrect password
```
This output will be in case when status code of request is 401. It means that provided password is incorrect.


Finally, in case when status code of request is 200:

* if there is at least one pull request:	

output will contain information of PR title, description, author who created PR, date and time of creation and link to PR itself.

* if there is no pull requests: 			

output will be like ```There is no Pull Requests sent to username.```
